package ru.t1.dzelenin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.dzelenin.tm.api.model.ICommand;
import ru.t1.dzelenin.tm.command.AbstractCommand;

import java.util.Collection;

public final class ArgumentListConnamd extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        @NotNull final Collection<AbstractCommand> commands = getCommandService().getCommands();
        for (ICommand command : commands) {
            if (command == null) continue;
            @NotNull final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument.toString());
        }
    }

    @NotNull
    @Override
    public String getName() {
        return "arguments";
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-arg";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show list arguments";
    }

}
