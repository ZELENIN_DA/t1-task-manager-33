package ru.t1.dzelenin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.dzelenin.tm.dto.request.task.TaskShowByIndexRequest;
import ru.t1.dzelenin.tm.dto.response.task.TaskShowByIndexResponse;
import ru.t1.dzelenin.tm.util.TerminalUtil;

public final class TaskShowByIndexCommand extends AbstractTaskShowCommand {

    private final String NAME = "task-show-by-index";

    private final String DESCRIPTION = "Display task by index.";

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final TaskShowByIndexRequest request = new TaskShowByIndexRequest(getToken(), index);
        @NotNull final TaskShowByIndexResponse response = getTaskEndpoint().showTaskByIndex(request);
        showTask(response.getTask());
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
