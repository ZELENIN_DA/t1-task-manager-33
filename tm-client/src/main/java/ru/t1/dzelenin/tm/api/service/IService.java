package ru.t1.dzelenin.tm.api.service;

import ru.t1.dzelenin.tm.api.repository.IRepository;
import ru.t1.dzelenin.tm.model.AbstractModel;

public interface IService<M extends AbstractModel> extends IRepository<M> {
}


