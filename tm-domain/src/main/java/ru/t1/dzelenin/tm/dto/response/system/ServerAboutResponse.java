package ru.t1.dzelenin.tm.dto.response.system;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.dzelenin.tm.dto.response.AbstractResponse;

@Getter
@Setter
public final class ServerAboutResponse extends AbstractResponse {

    @NotNull
    private String email;

    @NotNull
    private String name;

}
