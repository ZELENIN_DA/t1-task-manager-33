package ru.t1.dzelenin.tm.api.endpoint;


import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import javax.jws.WebMethod;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.URL;

public interface IEndpoint {

    @NotNull
    String HOST = "localhost";

    @NotNull
    String PORT = "8080";

    @NotNull
    String REQUEST = "request";

    @NotNull
    String NAMESPACE = "http://endpoint.tm.dzelenin.t1.ru/";

    @NotNull
    String SERVICE = "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static <T> T newInstance(
            @NotNull final IConnectionProvider connectionProvider,
            @NotNull final String name,
            @NotNull final String namespace,
            @NotNull final String part,
            @NotNull final Class<T> clazz
    ) {
        @NotNull final String host = connectionProvider.getHost();
        @NotNull final String port = connectionProvider.getPort();
        return newInstance(host, port, name, namespace, part, clazz);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static <T> T newInstance(
            @NotNull final String host,
            @NotNull final String port,
            @NotNull final String name,
            @NotNull final String namespace,
            @NotNull final String part,
            @NotNull final Class<T> clazz
    ) {
        @NotNull final String wsdl = String.format("http://%s:%s/%s?WSDL", host, port, name);
        @NotNull final URL url = new URL(wsdl);
        @NotNull final QName qName = new QName(namespace, part);
        return Service.create(url, qName).getPort(clazz);
    }

}
