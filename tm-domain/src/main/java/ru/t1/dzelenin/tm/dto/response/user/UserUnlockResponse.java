package ru.t1.dzelenin.tm.dto.response.user;

import lombok.NoArgsConstructor;
import ru.t1.dzelenin.tm.dto.response.AbstractResultResponse;

@NoArgsConstructor
public final class UserUnlockResponse extends AbstractResultResponse {
}
