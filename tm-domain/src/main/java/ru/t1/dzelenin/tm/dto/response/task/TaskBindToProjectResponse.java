package ru.t1.dzelenin.tm.dto.response.task;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class TaskBindToProjectResponse extends AbstractTaskResponse {
}
