package ru.t1.dzelenin.tm.endpoint;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.api.endpoint.IAuthEndpoint;
import ru.t1.dzelenin.tm.api.service.IAuthService;
import ru.t1.dzelenin.tm.api.service.IServiceLocator;
import ru.t1.dzelenin.tm.dto.request.user.UserLoginRequest;
import ru.t1.dzelenin.tm.dto.request.user.UserLogoutRequest;
import ru.t1.dzelenin.tm.dto.request.user.UserProfileRequest;
import ru.t1.dzelenin.tm.dto.response.user.UserLoginResponse;
import ru.t1.dzelenin.tm.dto.response.user.UserLogoutResponse;
import ru.t1.dzelenin.tm.dto.response.user.UserProfileResponse;
import ru.t1.dzelenin.tm.model.Session;
import ru.t1.dzelenin.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1.dzelenin.tm.api.endpoint.IAuthEndpoint")
public class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    public AuthEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IAuthService getAuthService() {
        return getServiceLocator().getAuthService();
    }

    @NotNull
    @Override
    @WebMethod
    public UserLoginResponse login(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final UserLoginRequest request) {
        @NotNull final IAuthService authService = getServiceLocator().getAuthService();
        @NotNull final String token = authService.login(request.getLogin(), request.getPassword());
        return new UserLoginResponse(token);
    }

    @NotNull
    @Override
    @WebMethod
    public UserLogoutResponse logout(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final UserLogoutRequest request) {
        @NotNull final Session session = check(request);
        getAuthService().logout(session);
        return new UserLogoutResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserProfileResponse profile(@WebParam(name = REQUEST, partName = REQUEST) @NotNull UserProfileRequest request) {
        @Nullable final Session session = check(request);
        @Nullable final User user = getServiceLocator().getUserService().findOneById(session.getUserId());
        return new UserProfileResponse(user);
    }

}
