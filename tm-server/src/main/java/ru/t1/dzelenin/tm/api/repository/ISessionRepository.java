package ru.t1.dzelenin.tm.api.repository;

import ru.t1.dzelenin.tm.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session> {

}