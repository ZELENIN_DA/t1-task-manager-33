package ru.t1.dzelenin.tm.repository;

import ru.t1.dzelenin.tm.api.repository.ISessionRepository;
import ru.t1.dzelenin.tm.model.Session;

public class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

}