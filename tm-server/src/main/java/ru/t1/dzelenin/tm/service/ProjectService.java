package ru.t1.dzelenin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.api.repository.IProjectRepository;
import ru.t1.dzelenin.tm.api.service.IProjectService;
import ru.t1.dzelenin.tm.enumerated.Status;
import ru.t1.dzelenin.tm.exception.entity.ProjectNotFoundException;
import ru.t1.dzelenin.tm.exception.entity.StatusEmptyException;
import ru.t1.dzelenin.tm.exception.field.*;
import ru.t1.dzelenin.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public final class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(final IProjectRepository repository) {
        super(repository);
    }

    @NotNull
    @Override
    public Project create(@NotNull final String userId, @NotNull final String name, @NotNull final String description) {
        if (userId == null || userId.isEmpty())
            throw new UserIdEmptyException();
        if (name == null || name.isEmpty())
            throw new NameEmptyException();
        if (description == null)
            throw new DescriptionEmptyException();
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        add(userId, project);
        return project;
    }

    @NotNull
    @Override
    public Project create(@NotNull final String userId, @NotNull final String name) {
        if (userId == null || userId.isEmpty())
            throw new UserIdEmptyException();
        if (name == null || name.isEmpty())
            throw new NameEmptyException();
        @NotNull final Project project = new Project();
        project.setName(name);
        add(userId, project);
        return project;
    }

    @Nullable
    @Override
    public List<Project> findAll(@NotNull final String userId, @Nullable final Comparator comparator) {
        return null;
    }

    @Nullable
    @Override
    public Project updateById(@NotNull final String userId,
                              @NotNull final String id,
                              @NotNull final String name,
                              @NotNull final String description) {
        if (userId == null || userId.isEmpty())
            throw new UserIdEmptyException();
        if (name == null || name.isEmpty())
            throw new NameEmptyException();
        @NotNull final Project project = findOneById(userId, id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Nullable
    @Override
    public Project updateByIndex(@NotNull final String userId,
                                 @NotNull final Integer index,
                                 @NotNull final String name,
                                 @NotNull final String description) {
        if (userId == null || userId.isEmpty())
            throw new UserIdEmptyException();
        if (name == null || name.isEmpty())
            throw new NameEmptyException();
        @NotNull final Project project = findOneByIndex(userId, index);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Nullable
    @Override
    public Project changeProjectStatusIndex(@NotNull final String userId,
                                            @NotNull final Integer index,
                                            @NotNull final Status status) {
        if (userId == null || userId.isEmpty())
            throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= repository.getSize())
            throw new IndexIncorrectException();
        if (status == null)
            throw new StatusEmptyException();
        @NotNull final Project project = findOneByIndex(userId, index);
        if (project == null)
            throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @Nullable
    @Override
    public Project changeProjectStatusId(@NotNull final String userId,
                                         @NotNull final String id,
                                         @NotNull final Status status) {
        if (userId == null || userId.isEmpty())
            throw new UserIdEmptyException();
        if (id == null || id.isEmpty())
            throw new ProjectIdEmptyException();
        if (status == null)
            throw new StatusEmptyException();
        @NotNull final Project project = findOneById(userId, id);
        if (project == null)
            throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @Nullable
    @Override
    public int getSize(@NotNull final String userId) {
        return repository.getSize();
    }

}

