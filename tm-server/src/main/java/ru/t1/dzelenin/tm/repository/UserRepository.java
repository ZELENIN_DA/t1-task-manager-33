package ru.t1.dzelenin.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.dzelenin.tm.api.repository.IUserRepository;
import ru.t1.dzelenin.tm.model.User;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    @Override
    public User findByLogin(@NotNull final String login) {
        return models.stream()
                .filter(m -> login.equals(m.getLogin()))
                .findFirst().orElse(null);
    }

    @NotNull
    @Override
    public User findByEmail(@NotNull final String email) {
        return models.stream()
                .filter(m -> email.equals(m.getEmail()))
                .findFirst().orElse(null);
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@NotNull final String login) {
        return models.stream().anyMatch(m -> login.equals(m.getLogin()));
    }

    @NotNull
    @Override
    public Boolean isMailExist(@NotNull final String email) {
        return models.stream().anyMatch(m -> email.equals(m.getEmail()));
    }

}



